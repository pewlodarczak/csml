using Microsoft.ML;
using Microsoft.ML.Data;
using static Microsoft.ML.DataOperationsCatalog;

class SentimentAnalysis
{
    static string _dataPath = Path.Combine(Environment.CurrentDirectory, "Data", "yelp_labelled.txt");
    //static string _dataPath = Path.Combine(Environment.CurrentDirectory, "Data", "imdb_labelled.txt");
    //static string _dataPath = Path.Combine(Environment.CurrentDirectory, "Data", "amazon_cells_labelled.txt");
    //static string _dataPath = Path.Combine(Environment.CurrentDirectory, "Data", "yelp_imdb_combined.txt");
    public static void StartAnalyze()
    {
        MLContext ctx = new MLContext();
        // TrainTestData splitDataView = LoadData(ctx);
        // ITransformer model = BuildAndTrainModel(ctx, splitDataView.TrainSet);
        // Evaluate(ctx, model, splitDataView.TestSet);
        // SaveModel(ctx, model);

        DataViewSchema predictionPipelineSchema;
        ITransformer predictionPipeline = ctx.Model.Load("Models/model.zip", out predictionPipelineSchema);
        //UseModelWithSingleItem(ctx, model);
        string sentence = "";
        do {
            Console.WriteLine("Enter a sentence:");
            sentence = Console.ReadLine();
            //UseModelWithSingleItem(ctx, model, sentence);
            UseModelWithSingleItem(ctx, predictionPipeline, sentence);
        } while (sentence != "");
        //UseModelWithBatchItems(ctx, model);
    }


    static TrainTestData LoadData(MLContext mlContext)
    {
        IDataView dataView = mlContext.Data.LoadFromTextFile<SentimentData>(_dataPath, hasHeader: false);
        TrainTestData splitDataView = mlContext.Data.TrainTestSplit(dataView, testFraction: 0.2);
        return splitDataView;
    }

    static void SaveModel(MLContext mlContext, ITransformer model)
    {
        IDataView dataView = mlContext.Data.LoadFromTextFile<SentimentData>(_dataPath, hasHeader: false);
        mlContext.Model.Save(model, dataView.Schema, "Models/model.zip");
        //using FileStream stream = File.Create("Models/onnx_model.onnx");
        //mlContext.Model.ConvertToOnnx(trainedModel, data, stream);
    }

    static ITransformer BuildAndTrainModel(MLContext mlContext, IDataView splitTrainSet)
    {   
        // Stochastic Dual Coordinate Ascent
        // var estimator = mlContext.Transforms.Text.FeaturizeText(outputColumnName: "Features", inputColumnName: nameof(SentimentData.SentimentText))
        // .Append(mlContext.BinaryClassification.Trainers.SdcaLogisticRegression(labelColumnName: "Label", featureColumnName: "Features"));

        // Limited-memory BFGS (Broyden–Fletcher–Goldfarb–Shanno algorithm)
        var estimator = mlContext.Transforms.Text.FeaturizeText(outputColumnName: "Features", inputColumnName: nameof(SentimentData.SentimentText))
        .Append(mlContext.BinaryClassification.Trainers.LbfgsLogisticRegression(labelColumnName: "Label", featureColumnName: "Features"));
        
        // var estimator = mlContext.Transforms.Text.FeaturizeText(outputColumnName: "Features", inputColumnName: nameof(SentimentData.SentimentText))
        // .Append(mlContext.BinaryClassification.Trainers.FieldAwareFactorizationMachine(labelColumnName: "Label", featureColumnName: "Features"));
        
        // var estimator = mlContext.Transforms.Text.FeaturizeText(outputColumnName: "Features", inputColumnName: nameof(SentimentData.SentimentText))
        // .Append(mlContext.BinaryClassification.Trainers.LinearSvm(labelColumnName: "Label", featureColumnName: "Features"));

        Console.WriteLine("=============== Create and Train the Model ===============");
        var model = estimator.Fit(splitTrainSet);
        Console.WriteLine("=============== End of training ===============");
        Console.WriteLine();
        return model;
    }

    static void Evaluate(MLContext mlContext, ITransformer model, IDataView splitTestSet)
    {
        Console.WriteLine("=============== Evaluating Model accuracy with Test data===============");
        IDataView predictions = model.Transform(splitTestSet);
        CalibratedBinaryClassificationMetrics metrics = mlContext.BinaryClassification.Evaluate(predictions, "Label");
        Console.WriteLine();
        Console.WriteLine("Model quality metrics evaluation");
        Console.WriteLine("--------------------------------");
        Console.WriteLine($"Accuracy: {metrics.Accuracy:P2}");
        Console.WriteLine($"Auc: {metrics.AreaUnderRocCurve:P2}");
        Console.WriteLine($"F1Score: {metrics.F1Score:P2}");
        Console.WriteLine("=============== End of model evaluation ===============");
    }

    static void UseModelWithSingleItem(MLContext mlContext, ITransformer model)
    {
        PredictionEngine<SentimentData, SentimentPrediction> predictionFunction = mlContext.Model.CreatePredictionEngine<SentimentData, SentimentPrediction>(model);
        SentimentData sampleStatement = new SentimentData
        {
            SentimentText = "This was a very bad steak"
        };
        var resultPrediction = predictionFunction.Predict(sampleStatement);
        Console.WriteLine();
        Console.WriteLine("=============== Prediction Test of model with a single sample and test dataset ===============");
        Console.WriteLine();
        Console.WriteLine($"Sentiment: {resultPrediction.SentimentText} | Prediction: {(Convert.ToBoolean(resultPrediction.Prediction) ? "Positive" : "Negative")} | Probability: {resultPrediction.Probability} ");
        Console.WriteLine("=============== End of Predictions ===============");
        Console.WriteLine();
    }

    static void UseModelWithSingleItem(MLContext mlContext, ITransformer model, string StrToEvaluate)
    {
        PredictionEngine<SentimentData, SentimentPrediction> predictionFunction = mlContext.Model.CreatePredictionEngine<SentimentData, SentimentPrediction>(model);
        SentimentData sampleStatement = new SentimentData
        {
            SentimentText = StrToEvaluate
        };
        var resultPrediction = predictionFunction.Predict(sampleStatement);
        Console.WriteLine();
        Console.WriteLine("=============== Prediction Test of model with a single sample and test dataset ===============");
        Console.WriteLine();
        Console.WriteLine($"Sentiment: {resultPrediction.SentimentText} | Prediction: {(Convert.ToBoolean(resultPrediction.Prediction) ? "Positive" : "Negative")} | Probability: {resultPrediction.Probability} ");
        Console.WriteLine("=============== End of Predictions ===============");
        Console.WriteLine();
    }


    static void UseModelWithBatchItems(MLContext mlContext, ITransformer model)
    {
        IEnumerable<SentimentData> sentiments = new[]
        {
            new SentimentData
            {
                SentimentText = "This was a horrible meal"
            },
            new SentimentData
            {
                SentimentText = "I love this spaghetti."
            }
        };
        IDataView batchComments = mlContext.Data.LoadFromEnumerable(sentiments);
        IDataView predictions = model.Transform(batchComments);
        IEnumerable<SentimentPrediction> predictedResults = mlContext.Data.CreateEnumerable<SentimentPrediction>(predictions, reuseRowObject: false);
        Console.WriteLine();
        Console.WriteLine("=============== Prediction Test of loaded model with multiple samples ===============");
        foreach (SentimentPrediction prediction  in predictedResults)
        {
            Console.WriteLine($"Sentiment: {prediction.SentimentText} | Prediction: {(Convert.ToBoolean(prediction.Prediction) ? "Positive" : "Negative")} | Probability: {prediction.Probability} ");
        }
        Console.WriteLine("=============== End of predictions ===============");
    }
}
